Logging In C++
----

> This code is taken from a drdobbs article by Petru Marginean

Logging is a critical technique for troubleshooting and maintaining software systems. Petru presents a C++ logging framework that is typesafe, thread-safe, and portable.

Logging is a critical technique for troubleshooting and maintaining software systems. It's simple, provides information without requiring knowledge of programming language, and does not require specialized tools. Logging is a useful means to figure out if an application is actually doing what it is supposed to do. Good logging mechanisms can save long debugging sessions and dramatically increase the maintainability of applications.

By Petru Marginean
September 05, 2007

More info in here[http://www.drdobbs.com/cpp/logging-in-c/201804215]
